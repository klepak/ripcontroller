## ASCII EASY


### Voyage destination

    call execve("/bin/sh", 0, 0)

### Approach

    1. store address to '/bin/sh' in ebx - use 'add_ebx' gadget.
    2. use pushal and pops to store move ebx to esi.
    3. use mov_eax_esi_add_esp to store esi into eax
    4. prepare stack: (&/bin/sh, 0, 0)
    5. jump to 0x55616767:
               0x55616767   mov dword [esp], eax
               0x5561676a   call sym.execve


See final exploit [here](./ascii_easy.py)
