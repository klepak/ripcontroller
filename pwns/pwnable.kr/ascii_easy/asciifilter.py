#!/usr/bin/python

"""

Filter for ropper output. Prints only addressable gadgets,
 in terms of ASCII charset range.
 Usage:

 $ ropper --file ./libc-2.15.so -I 0x5555e000 | ./asciifilter.py

"""

import fileinput

ASCII_LOW  = 0x20
ASCII_HIGH = 0x7f

for line in fileinput.input():

	ropperaddr = line.split(':')[0]

	try:
		addr = int(ropperaddr[7:len(ropperaddr)-4], 0)
	except:
		continue

	addressable = True
	for r in range(0,4):
		byte = (addr >> r*8) & 0xFF
		if not ASCII_LOW <= byte <= ASCII_HIGH:
			addressable = False
			break
	if addressable:
		print(line.split('\n')[0])
