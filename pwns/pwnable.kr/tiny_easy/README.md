## Tiny Easy

### user-controller-input

1. argv 
3. environment variables

### Observations

```
$ file ./tiny_easy 
./tiny_easy: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), statically linked, corrupted section header size
$
```

Hm. Shellcode via ENV?

### ELF Headers as assembly

```
gdb-peda$ x/50i 0x08048000                                                                                                                                                                                  
   0x8048000:	jg     0x8048047
   0x8048002:	dec    esp
   0x8048003:	inc    esi
   0x8048004:	add    DWORD PTR [ecx],eax
   0x8048006:	add    DWORD PTR [eax],eax
   0x8048008:	add    BYTE PTR [eax],al
   0x804800a:	add    BYTE PTR [eax],al
   0x804800c:	add    BYTE PTR [eax],al
   0x804800e:	add    BYTE PTR [eax],al
   0x8048010:	add    al,BYTE PTR [eax]
   0x8048012:	add    eax,DWORD PTR [eax]
   0x8048014:	add    DWORD PTR [eax],eax
   0x8048016:	add    BYTE PTR [eax],al
   0x8048018:	push   esp
   0x8048019:	add    BYTE PTR [eax+ecx*1],0x34
   0x804801d:	add    BYTE PTR [eax],al
   0x804801f:	add    BYTE PTR [eax],al
   0x8048021:	add    BYTE PTR [eax],al
   0x8048023:	add    BYTE PTR [eax],al
   0x8048025:	add    BYTE PTR [eax],al
   0x8048027:	add    BYTE PTR [eax+eax*1],dh
   0x804802a:	and    BYTE PTR [eax],al
   0x804802c:	add    DWORD PTR [eax],eax
   0x804802e:	add    BYTE PTR [eax],al
   0x8048030:	add    BYTE PTR [eax],al
   0x8048032:	add    BYTE PTR [eax],al
   0x8048034:	add    DWORD PTR [eax],eax
   0x8048036:	add    BYTE PTR [eax],al
   0x8048038:	add    BYTE PTR [eax],al
   0x804803a:	add    BYTE PTR [eax],al
   0x804803c:	add    BYTE PTR [eax-0x7ffff7fc],al
   0x8048042:	add    al,0x8
   0x8048044:	pop    edx
   0x8048045:	add    BYTE PTR [eax],al
   0x8048047:	add    BYTE PTR [edx+0x0],bl
   0x804804a:	add    BYTE PTR [eax],al
   0x804804c:	add    eax,0x0
   0x8048051:	adc    BYTE PTR [eax],al
   0x8048053:	add    BYTE PTR [eax+0x5a],bl
   0x8048056:	mov    edx,DWORD PTR [edx]
=> 0x8048058:	call   edx
```

We could store alphanumeric shellcode on the stack (via environment variables), 
however there is the ASLR.
